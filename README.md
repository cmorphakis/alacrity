# AlacrityLaw coding assignment

## General 
The service uses a containerized Mysql 5.7 docker image. Because of that you will need to have Docker and Docker-compose
installed on your system before running the service.

Since this is a Node.js service, Node will also need to be installed alongside its package manager `npm`. Although the 
service is backward-compatible with previous versions of node, a version >= v12.x is recommended. 


The service runs by default on port 3003 and exposes two endpoints as described by the requirements document:
- http://localhost:3003/clients/insert
- http://localhost:3003/clients/retrieve

Both of the requests are of POST type.

## Installation
After installing Docker, Node.js and npm, you can go ahead and install the service's dependencies by running the 
terminal command from the project's root directory:
```$xslt
npm install
```

## Starting the service
The first step is to start the docker service. From your terminal:
```$xslt
docker-compose up -d
```
This will start the mysql server as a container service running in the background. All data will be persisted in the 
`mysql-container/data` folder. This will make sure that even if the docker service stops, the data will not be lost.

 After the mysql service starts, you can go ahead and initialize the express app:
```$xslt
npm start
```

## Running the tests
You can run the tests locally using the command:
```$xslt
npm test
```
