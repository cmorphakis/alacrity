CREATE DATABASE IF NOT EXISTS alacrity;
USE alacrity;

CREATE TABLE IF NOT EXISTS `alacrity`.`client_data` (
  `client_data_id` VARCHAR(512) NOT NULL,
  `client_data_group` VARCHAR(512) NOT NULL,
  `data` TEXT NOT NULL,
  `date_created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `date_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`client_data_id`),
  INDEX `GROUP` (`client_data_group`)
);




