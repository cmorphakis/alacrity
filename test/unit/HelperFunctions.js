const assert = require('assert');
const {describe, it} = require('mocha');
const HelperFunctions = require('../../src/utils/helper-functions');

describe('HelperFunctions tests', () =>  {

    describe('getGroupFromId()', () => {
        const input = ['test-group-id1', 'test-id2', 'test-group-aa-id3', 'test_no_group'];
        const expectedOutput = ['test-group', 'test', 'test-group-aa', null];

        input.forEach((val, i) => {
            const group = HelperFunctions.getGroupFromId(val);
            it(`getGroupFromId should return ${expectedOutput[i]} for input ${val}`, () => {
                assert.strictEqual(group, expectedOutput[i]);
            })
        })
    });

    describe('isGroup()', () => {
        const input = ['test-group-id1-*', 'test-id2', 'test-group-aa-id3-*', 'test-*', '-*'];
        const expectedOutput = [true, false, true, true, false];

        input.forEach((val, i) => {
            const group = HelperFunctions.isGroup(val);
            it(`isGroup should return ${expectedOutput[i]} for input ${val}`, () => {
                assert.strictEqual(group, expectedOutput[i]);
            })
        })
    });

    describe('encrypt()/decrypt()', () => {
        const dotenv = require('dotenv');
        dotenv.config();

        const textToEncrypt = 'this is the test to encrypt';
        const encryptionKey = '12345678123456781234567812345678'
        const cipherText = HelperFunctions.encrypt(encryptionKey, textToEncrypt);

        const plainText = HelperFunctions.decrypt(encryptionKey, cipherText);

        it ('should return the same output as the input', () => {
            assert.strictEqual(plainText, textToEncrypt);
        });
    });


});
