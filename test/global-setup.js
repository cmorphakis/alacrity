const {before, after} =  require('mocha');
const {start, stop} = require('../bin/www');

let server;
before( () => {
    server = start();
});

after( () => {
    stop(server);
});


