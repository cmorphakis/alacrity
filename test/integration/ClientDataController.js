const assert = require('assert');
const {describe, it} = require('mocha');
const dotenv = require('dotenv');

const ClientDataController = require('../../src/controllers/ClientDataController');
const HelperFunctions = require('../../src/utils/helper-functions');
const ClientData = require('../../src/models').ClientData;
dotenv.config();

describe('ClientDataController tests', () =>  {
    const encryptionKey = '12345678123456781234567812345678';
    const testId = `thisianceid-${new Date().getTime()}`;
    let objToAdd;
    describe('insertOrUpdate()', async () => {
        objToAdd = {
            id: testId,
            encryption_key: encryptionKey,
            value: {'key1': 1, 'key2': 'string', 'key3' : {'innerKey:': 4}}
        };
        const dotenv = require('dotenv');
        dotenv.config();

        it ('should return true for a newly created record', async () => {
            const res = await ClientDataController.insertOrUpdate(objToAdd);
            assert.strictEqual(res, true );

            const objects = await ClientData.findAll({
                where: {
                    client_data_id: HelperFunctions.encrypt(encryptionKey, objToAdd.id)
                }
            });

            const dateCreated = new Date(objects[0].dataValues.date_created).getTime();
            const dateUpdated = new Date(objects[0].dataValues.date_updated).getTime();
            assert.strictEqual(dateCreated, dateUpdated);
        });
        it ('should return false for an updated record', async () => {
            objToAdd.value = {'b': 3};
            setTimeout( async () => {

                const res = await ClientDataController.insertOrUpdate(objToAdd);
                assert.strictEqual(res, false );
                const objects = await ClientData.findAll({
                    where: {
                        client_data_id: HelperFunctions.encrypt(encryptionKey, objToAdd.id)
                    }
                });

                const dateCreated = new Date(objects[0].dataValues.date_created).getTime();
                const dateUpdated = new Date(objects[0].dataValues.date_updated).getTime();

                // date_updated should be greater than date_created
                assert.strictEqual(dateUpdated > dateCreated, true);
                // updated object should have the updated data
                assert.deepStrictEqual(objToAdd.value,  JSON.parse(HelperFunctions.decrypt(encryptionKey, objects[0].dataValues.data)));
            }, 2000);
        });
    });

    describe('getObjectById()', () => {

        it ('should have the correct values after the update', async () => {
            const objArray = await ClientDataController.getObjectById(encryptionKey, testId);
            console.log('aAAA', objArray);
            assert.strictEqual(objArray[0].client_data_id, objToAdd.id);
        });
    });

    describe('getObjectsByGroup()', async () => {
        const baseGroup = `group2_${new Date().getTime()}`;

        const objToAdd2 = {
            id: baseGroup + '-1',
            encryption_key: encryptionKey,
            value: {'key1': 1, 'key2': 'string', 'key3' : {'innerKey:': 4}}
        };
        const objToAdd3 = {
            id: baseGroup + '-2',
            encryption_key: encryptionKey,
            value: {'key1': 1, 'key2': 'string', 'key3' : {'innerKey:': 4}}
        };

        it ('should return all the elements belonging to the group', async () => {
            await ClientDataController.insertOrUpdate(objToAdd2);
            await ClientDataController.insertOrUpdate(objToAdd3);

            const res = await ClientDataController.getObjectsByGroup(encryptionKey, baseGroup);

            console.log('RES', res);

            assert.strictEqual(res.length, 2);

        });
    });

    // TODO :: create after() hooks that cleanup all the created db-records during testing
});
