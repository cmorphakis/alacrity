'use strict';
module.exports = (sequelize, DataTypes) => {
  const ClientData = sequelize.define('ClientData', {
    client_data_id: {
      type: DataTypes.STRING,
      primaryKey: true
    },
    client_data_group: DataTypes.STRING,
    data: DataTypes.STRING,
    date_created: {
      type: 'TIMESTAMP',
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
    },
    date_updated: {
      type: 'TIMESTAMP',
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
    },
  }, {
    tableName: 'client_data',
    timestamps: false
  });

  return ClientData;
};
