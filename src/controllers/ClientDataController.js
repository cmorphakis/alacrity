const HelperFunctions = require('../utils/helper-functions');
const ClientData = require('../models').ClientData;
const resources = require('../resources/error-messages');

/**
 *
 * @param body The body of the POST request
 * @param {string} body.id  The unique id to store the data on
 * @param {string} body.encryption_key  The encryption key to encrypt data with
 * @param {Object} body.value  Any valid JSON object
 */
async function insertOrUpdate(body) {
    const objForDbInsert = getEncryptedObject(body);
    const res = await ClientData.upsert(objForDbInsert);
    return res;
}

/**
 *
 * @param {string} decryptionKey
 * @param {string} id
 * @returns {Promise<{data: Object, client_data_id: string}[]>}
 */
async function getObjectById(decryptionKey, id) {
    const encryptedId = HelperFunctions.encrypt(decryptionKey, id);
    const objects = await ClientData.findAll({
        where: {
            client_data_id: encryptedId
        }
    });
    if (objects.length === 0) {
        throw new Error(resources.INVALID_RETRIEVAL_REQUEST);
    }
    return [getDecryptedObject(decryptionKey, objects[0])];

}

/**
 *
 * @param {string} decryptionKey
 * @param {string} group
 * @returns {Promise<{data: Object, client_data_id: string}[]>}
 */
async function getObjectsByGroup(decryptionKey, group) {
    const encryptedGroup = HelperFunctions.encrypt(decryptionKey, group);
    const objects = await ClientData.findAll({
        where: {
            client_data_group: encryptedGroup
        }
    });
    if (objects.length === 0) {
        throw new Error(resources.INVALID_RETRIEVAL_REQUEST);
    }
    return objects.map(obj => {
        return getDecryptedObject(decryptionKey, obj);
    });
}

/**
 *
 * @param body The body of the POST request
 * @param {string} body.id  The unique id to store the data on
 * @param {string} body.encryption_key  The encryption key to encrypt data with
 * @param {Object} body.value  Any valid JSON object
 */
function getEncryptedObject(body) {
    const {id, encryption_key, value} = body;
    const group = HelperFunctions.getGroupFromId(body.id);

    const objForDbInsert = {
        client_data_id: HelperFunctions.encrypt(encryption_key, id),
        client_data_group: HelperFunctions.encrypt(encryption_key, group),
        data: HelperFunctions.encrypt(encryption_key, JSON.stringify(value))
    };

    return objForDbInsert;
}

/**
 *
 * @param {string} encryption_key
 * @param dbOject
 * @returns {{data: any, client_data_id: string}}
 */
function getDecryptedObject(encryption_key, dbOject) {

    const decryptedObject = {
        client_data_id: HelperFunctions.decrypt(encryption_key, dbOject.client_data_id),
        data: JSON.parse(HelperFunctions.decrypt(encryption_key, dbOject.data).toString())
    };

    return decryptedObject;
}

module.exports = {
    insertOrUpdate,
    getObjectsByGroup,
    getObjectById,
};
