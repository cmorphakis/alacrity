const express = require('express');
const expressJoi = require('express-joi-validator');
const winston = require('winston');

const {insertSchema, getSchema, joiOptions} = require('../utils/route-validators');
const HelperFunctions = require('../utils/helper-functions');
const ClientDataController = require('../controllers/ClientDataController');
const resources = require('../resources/error-messages');

const router = express.Router();

router.post('/insert', expressJoi(insertSchema, joiOptions), (req, res, next) => {
    return errorHandlerWrapper(req, res, next, async () => {
        const result = await ClientDataController.insertOrUpdate(req.body);
        return {result};
    });
});

router.post('/retrieve', expressJoi(getSchema, joiOptions), (req, res, next) => {
    return errorHandlerWrapper(req, res, next, async () => {
        const {id, decryption_key} = req.body;
        const isGroupRequest = HelperFunctions.isGroup(id);

        if (isGroupRequest) {
            const group = HelperFunctions.getGroupFromId(id);
            const data = await ClientDataController.getObjectsByGroup(decryption_key, group);

            return {data};
        } else {
            const data = await ClientDataController.getObjectById(decryption_key, id);

            return {data};
        }
    });
});

module.exports = router;


async function errorHandlerWrapper(req, res, next, handler) {
    try {
        const handlerResult = await handler();
        return res.send(handlerResult);
    } catch(error) {
        winston.error('Error:', error);
        return next(error);
    }
}
