const express = require('express');

const clientRouter = require('./routes/client.router');
const resources = require('./resources/error-messages');
const HelperFunctions = require('./utils/helper-functions');

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

HelperFunctions.setupLoggers(app);

app.use('/clients', clientRouter);


// handle all requests to undefined routes **NOTE** This should always below the routers setup
app.use((req, res, next) => {
    res.status(404);
    next(new Error(resources.ROUTE_NOT_FOUND));
});

app.use((error, req, res, next) => {
    // wrong route error
    if (error.message === resources.ROUTE_NOT_FOUND) {
        return res.status(404).send({error: error.message});
    }
    // no matched records based on the encryption key provided
    if (error.message === resources.INVALID_RETRIEVAL_REQUEST) {
        return res.status(200).send({data: []});
    }
    // this is Joi validation error
    if (error.message === resources.INVALID_REQUEST_BODY) {
        return res.status(400).send({error: error.message});
    }
    if (error.isBoom) {
        return res.status(400).send({errors: error.data});
    } else {
        // handle everything else as a Server error and provide a trivial error message
        return res.status(500).send({error: resources.SERVER_ERROR});
    }
});

module.exports = app;


