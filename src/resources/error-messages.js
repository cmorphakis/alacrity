
const ROUTE_NOT_FOUND = 'Route not found';
const INVALID_RETRIEVAL_REQUEST = 'The decryption key provided matched no records';
const SERVER_ERROR = 'Internal Server Error';
const INVALID_REQUEST_BODY = 'Missing request body from request';

module.exports = {
    ROUTE_NOT_FOUND,
    INVALID_RETRIEVAL_REQUEST,
    SERVER_ERROR,
    INVALID_REQUEST_BODY
};
