const crypto = require('crypto');
const winston = require('winston');
const morgan = require('morgan');
const DailyRotateFile = require('winston-daily-rotate-file');


const config = {
    AES256: 'aes-256-cbc'
};


/**
 *
 * @param {string} id (the id received from the client request)
 * @returns {null|string} (parent-group of that id or null if no regex does not match)
 */
function getGroupFromId(id) {
    const regexResult = id.match(/(^.+)(-[^-.]+)$/);
    if (Array.isArray(regexResult)) {
        return regexResult[1];
    }
    return null;
}

function isGroup(id) {
    return /^(.+)-\*$/.test(id);
}

/**
 *
 * @param {string} key (The encryption key that will be used. Must be 32 chars long (the AES256))
 * @param {string} plaintext (The string to be encrypted)
 * @returns {string} (The encrypted text)
 */
function encrypt(key, plaintext) {
    const salt = process.env.HASH_SALT;
    const cipher = crypto.createCipheriv(config.AES256, key, salt);

    let encryptedText = cipher.update(plaintext, 'utf8', 'base64');

    encryptedText += cipher.final('base64');
    return encryptedText;
}

/**
 *
 * @param {string} key (The decryption key that will be used. Must be 32 chars long (the AES256))
 * @param {string} encryptedText (The string to be decrypted)
 * @returns {string} (The decrypted string)
 */
function decrypt(key, encryptedText) {
    const salt = process.env.HASH_SALT;
    let decipher = crypto.createDecipheriv(config.AES256, key, salt);
    let decrypted = decipher.update(encryptedText, 'base64');
    decrypted += decipher.final().toString();

    return decrypted;
}

/**
 * Sets up winston logger to output all logging events under daily-rotated files in the /logs folder.
 *
 * @param {Express.App} app (The instance of the express App)
 */
function setupLoggers(app) {
    const winstonMorganLogger = winston.createLogger();

    // log all requests that reach the express server
    winstonMorganLogger.add(
        new DailyRotateFile({
            filename: 'logs/http-requests-log-%DATE%.log',
            datePattern: 'YYYY-MM-DD',
            json: false
        })
    );

    // log everything else in a different file
    winston.add(
        new DailyRotateFile({
            format: winston.format.combine(
                winston.format.timestamp(),
                winston.format.json()),
            filename: 'logs/service-log-%DATE%.log',
            datePattern: 'YYYY-MM-DD',
            json: false
        })
    );

    winston.add(new winston.transports.Console({ format: winston.format.combine(
            winston.format.timestamp(),
            winston.format.simple()
        ) }));

    const morganStream = {
        write: (text) => {
            winstonMorganLogger.info(text.trim()); // proxy everything to winston
        }
    };

    app.use(morgan('combined', { stream: morganStream }));
    app.use(morgan('short'));

    winston.info('the loggers have been set successfully...');
}


module.exports = {
    getGroupFromId,
    isGroup,
    encrypt,
    decrypt,
    setupLoggers
};

