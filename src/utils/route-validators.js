const Joi = require('joi');

joiOptions = {
  abortEarly: false
};

/**
 * The validator used for the data insertion endpoint
 */
const insertSchema = {
    body: {
        id: Joi.string().regex(/(^.+)(-[^-.]+)$/).required(),
        encryption_key: Joi.string().length(32).required(),
        value: Joi.object().required()
    }
};

/**
 * The validator used for the data retrieval endpoint
 */
const getSchema = {
    body: {
        id: Joi.string().regex(/(^.+)(-[^-.]+)$/).required(),
        decryption_key: Joi.string().length(32).required(),
    }
};

module.exports = {
    insertSchema,
    getSchema,
    joiOptions
};
